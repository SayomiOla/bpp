/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bpp.servlet;

import com.bpp.hibernate.Scoring;
import com.bpp.hibernate.ScoringHibernateHelper;
import com.bpp.hibernate.ProgrammeSegmentHeader3;
import com.bpp.hibernate.ProgrammeSegmentHeader3HibernateHelper;
import com.bpp.hibernate.UsersHibernateHelper;
import com.bpp.utility.Utility;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Ola
 */
//@WebServlet(name = "ScoringServlet", urlPatterns = {"/ScoringServlet"})
public class ScoringServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, NoSuchAlgorithmException, MessagingException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession(true);
        try {
            UsersHibernateHelper userhelper = new UsersHibernateHelper();
            userhelper.saveUserIdSession(Integer.parseInt(session.getAttribute("userid").toString()));
            ScoringHibernateHelper helper = new ScoringHibernateHelper();
            ProgrammeSegmentHeader3HibernateHelper h3 = new ProgrammeSegmentHeader3HibernateHelper();

            Scoring scoring = new Scoring();
            Utility utility = new Utility();

            String option;
            String id;
            String project_code;
            String project_year;
            String criteria_id;
            String score;
            String date_created;
            String org_id;
            Double criteria1, criteria2, criteria3, criteria4, criteria5;
            String budgetyear;

            String resp = null;

            option = request.getParameter("option");
            if (option == null) {
                option = "";
            }

            id = request.getParameter("id");
            if (id == null) {
                id = "";
            }

            project_code = request.getParameter("project_code");
            if (project_code == null) {
                project_code = "";
            }

            project_year = request.getParameter("project_year");
            if (project_year == null) {
                project_year = "";
            }

            criteria_id = request.getParameter("criteria_id");
            if (criteria_id == null) {
                criteria_id = "";
            }

            score = request.getParameter("score");
            if (score == null) {
                score = "0";
            }

            date_created = request.getParameter("date_created");
            if (date_created == null) {
                date_created = "";
            }

            org_id = request.getParameter("org_id");
            if (org_id == null) {
                org_id = "";
            }
            budgetyear = request.getParameter("budgetyear");
            if (budgetyear == null) {
                budgetyear = "";
            }

            if (option.equals(Utility.OPTION_RESET_COOKIE)) {
                createCookie(response, id, "");
            }
            if (option.equals(Utility.OPTION_INSERT)) {
                criteria1 = Double.parseDouble(request.getParameter("criteria_id1"));
                criteria2 = Double.parseDouble(request.getParameter("criteria_id2"));
                criteria3 = Double.parseDouble(request.getParameter("criteria_id3"));
                criteria4 = Double.parseDouble(request.getParameter("criteria_id4"));
                criteria5 = Double.parseDouble(request.getParameter("criteria_id5"));
                resp = Utility.ActionResponse.RECORD_EXISTS.toString();
                int idvar = Integer.parseInt(helper.getMaxSerialNo("Scoring"));
                criteria_id = criteria1.toString();
                //System.out.println("**~~~~" + idvar + " " +  project_code + " " + criteria_id  + " " + project_year  + " " + score);
                List scoringList = helper.exists(project_code, Integer.parseInt(project_year));
                //scoring = helper.exists(idvar + 1, project_code, Integer.parseInt(project_year));
                if (scoringList != null) {
                    synchronized (this) {

                        scoring = new Scoring();
                        scoring.setId(idvar + 1);
                        scoring.setProjectCode(project_code);
                        scoring.setProjectYear(Integer.parseInt(project_year));
                        scoring.setScore(Double.parseDouble(score));
                        scoring.setDateCreated(utility.dbDateNow());
                        scoring.setOrgId(1);
                        Double Average = 0.0;
                        int cnt = countList(scoringList);
                        if (criteria1 == -1) { //Update or Insert Criteria 5 only
                            //New Project,               
                            Scoring sc = (Scoring) scoringList.get(0);
                            if (sc.getCriteriaId() == 5) {
                                //Update
                                //scoring.setCriteria_id(5);
                                //scoring.setScore(criteria5);
                                sc.setScore(criteria5);
                                resp = helper.update(sc);
                            } else {
                                scoring.setCriteriaId(5);
                                scoring.setScore(criteria5);
                                resp = helper.Insert(scoring);
                            }
                            Average = criteria5;
                            ProgrammeSegmentHeader3 ps3 = new ProgrammeSegmentHeader3();
                            String ps3code = project_code.substring(2);
                            ps3 = h3.fetchOnebyCode(ps3code);
                            ps3.setScore(Average);
                            h3.update(ps3);
                            h3.Rank(Integer.parseInt(budgetyear));
                        } else if (criteria5 == -1) { //Update or Insert Criteria 5 only
                            if (cnt == 1) {//Insert  
                                scoring.setCriteriaId(1);
                                scoring.setScore(criteria1);
                                resp = helper.Insert(scoring);
                                scoring.setCriteriaId(2);
                                scoring.setScore(criteria2);
                                scoring.setId(idvar + 2);
                                resp = helper.Insert(scoring);
                                scoring.setCriteriaId(3);
                                scoring.setScore(criteria3);
                                scoring.setId(idvar + 3);
                                resp = helper.Insert(scoring);
                                scoring.setCriteriaId(4);
                                scoring.setId(idvar + 4);
                                scoring.setScore(criteria4);
                                resp = helper.Insert(scoring);

                            } else {//Update
                                Scoring sc = null;
                                //Criteria 1
                                sc = (Scoring) scoringList.get(4);
                                sc.setScore(criteria1);
                                resp = helper.update(sc);
                                //Criteria 2
                                sc = (Scoring) scoringList.get(3);
                                sc.setScore(criteria2);
                                resp = helper.update(sc);
                                //Criteria 3
                                sc = (Scoring) scoringList.get(2);
                                sc.setScore(criteria3);
                                resp = helper.update(sc);
                                //Criteria 1
                                sc = (Scoring) scoringList.get(1);
                                sc.setScore(criteria4);
                                resp = helper.update(sc);
                            }
                            Average = (criteria1 + criteria2 + criteria3 + criteria4) / 4;
                        }
                        //Update PSH3

                        ProgrammeSegmentHeader3 ps3 = new ProgrammeSegmentHeader3();
                        String ps3code = project_code.substring(2);
                        ps3 = h3.fetchOnebyCode(ps3code);
                        ps3.setScore(Average);
                        h3.update(ps3);
                        h3.Rank(Integer.parseInt(budgetyear));
                    }
                } else {
                    //No Records update criteria 5 only
                    scoring.setId(idvar + 1);
                    scoring.setProjectCode(project_code);
                    scoring.setProjectYear(Integer.parseInt(project_year));
                    scoring.setDateCreated(utility.dbDateNow());
                    scoring.setCriteriaId(5);
                    scoring.setScore(criteria5);
                    resp = helper.Insert(scoring);
                    resp = helper.update(scoring);

                    ProgrammeSegmentHeader3 ps3 = new ProgrammeSegmentHeader3();
                    String ps3code = project_code.substring(2);
                    System.out.println("ps3code " + ps3code);
                    ps3 = h3.fetchOnebyCode(ps3code);
                    ps3.setScore(criteria5);
                    h3.update(ps3);
                    h3.Rank(Integer.parseInt(budgetyear));
                    //h3.update(ps3);
                }
                // Gson gson = new Gson();
                // resp = gson.toJson(resp);
                resp = Utility.ActionResponse.INSERTED.toString();
            }

            if (option.equals(Utility.OPTION_UPDATE)) {
                resp = Utility.ActionResponse.NO_RECORD.toString();
                scoring = helper.exists(Integer.parseInt(id));
                if (scoring != null) {
                    synchronized (this) {
                        scoring.setProjectCode(project_code);
                        scoring.setProjectYear(Integer.parseInt(project_year));
                        scoring.setCriteriaId(Integer.parseInt(criteria_id));
                        scoring.setScore(Double.parseDouble(score));
                        resp = helper.update(scoring);
                    }
                }
//                Gson gson = new Gson();
//                resp = gson.toJson(resp);
            }

            if (option.equals(Utility.OPTION_DELETE)) {
                synchronized (this) {
                    scoring = helper.exists(Integer.parseInt(id));
                    resp = helper.delete(scoring);
                }
//                Gson gson = new Gson();
//                resp = gson.toJson(resp);
            }

            if (option.equals(Utility.OPTION_SELECT_ALL)) {
                resp = helper.fetchAll(project_code);
            }

            if (option.equals(Utility.OPTION_SELECT_A_RECORD)) {
                resp = helper.fetchOne(id);
            }
            out.println(resp);

        } finally {
            out.close();
        }
    }

    public int countList(List itemList) {
        int count = 0;
        for (Object i : itemList) {
            count++;
        }
        return count;
    }

    public synchronized String readCookie(HttpServletRequest request, String cookiename) {
        Cookie[] cookies = request.getCookies();
        String resp = "";
        for (Cookie c : cookies) {
            if (c.getName().equals(cookiename)) {
                resp = c.getValue();
            }
        }
        return resp;
    }

//    public synchronized String readCookieValues(HttpServletRequest request, String cookiename) {
//        Cookie[] cookies = request.getCookies();
//        String resp = "";
//        for (Cookie c : cookies) {
//            if (c.getName().equals(cookiename)) {
//                resp = "value: " + c.getValue() + "   name: " + c.getName() + "   domain: " + c.getDomain() + "   path: " + c.getPath();
//            }
//        }
//        return resp;
//    }
    public synchronized void createCookie(HttpServletResponse response, String cookiename, String cookievalue) {
        Cookie c = new Cookie(cookiename, cookievalue);
        c.setMaxAge(24 * 60 * 60);
        response.addCookie(c);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ScoringServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(ScoringServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ScoringServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(ScoringServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
