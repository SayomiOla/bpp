/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bpp.hibernate;

import com.bpp.utility.Utility;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Transaction;

/**
 *
 * @author Adewale
 */
public class YearBudgetHibernateHelper {

    public static final String TABLE_NAME = "YearBudget";
    public static final String RAW_TABLE_NAME = "Year_Budget";

    /* YearBudget methods begin */
    public synchronized String insert(YearBudget yearBudget) {
        //YearBudget checkRole = exists(yearBudget.getAdminSegment(), yearBudget.getEconomicSegment());
        YearBudget checkRole = exists(yearBudget.getAdminSegment(), yearBudget.getEconomicSegment(), yearBudget.getProgrammeSegment(),
                yearBudget.getFunctionalSegment(), yearBudget.getFundSegment(), yearBudget.getGeoSegment(), yearBudget.getDeptId(), yearBudget.getBudgetYearId());

        if (checkRole == null) {
            final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            Transaction tx = null;
            try {
                //final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
                tx = session.beginTransaction();
                //session.beginTransaction();
                session.save(yearBudget);
                //session.getTransaction().commit();

                tx.commit();
            } catch (HibernateException e) {
                if (tx != null) {
                    tx.rollback();
                }
                e.printStackTrace();
                return "";
            } finally {
                //session.close();
            }
            return Utility.ActionResponse.INSERTED.toString();
        } else {
            return Utility.ActionResponse.RECORD_EXISTS.toString();
        }
    }

    public synchronized String update(YearBudget yearBudget) {
        YearBudget checkRole = exists(yearBudget.getId());
        if (checkRole != null) {
            final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            Transaction tx = null;
            try {
                //final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
                tx = session.beginTransaction();
                //session.beginTransaction();
                session.merge(yearBudget);
                //session.getTransaction().commit();

                tx.commit();
            } catch (HibernateException e) {
                System.out.println("error: " + e.getMessage());
                if (tx != null) {
                    tx.rollback();
                }
                e.printStackTrace();
                return "";
            } finally {
                //session.close();
            }
            return Utility.ActionResponse.UPDATED.toString();
        } else {

            return Utility.ActionResponse.NO_RECORD.toString();
        }
    }

    public synchronized String delete(YearBudget yearBudget) {
        YearBudget checkRole = exists(yearBudget.getId());
        if (checkRole != null) {
            final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            Transaction tx = null;
            try {
                //final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
                tx = session.beginTransaction();
                //session.beginTransaction();
                session.delete(yearBudget);
                //session.getTransaction().commit();

                tx.commit();
            } catch (HibernateException e) {
                if (tx != null) {
                    tx.rollback();
                }
                e.printStackTrace();
                return "";
            } finally {
                //session.close();
            }

            return Utility.ActionResponse.DELETED.toString();
        } else {

            return Utility.ActionResponse.NO_RECORD.toString();
        }
    }

    public synchronized YearBudget exists(String admin_segment, String economic_segment, String programme_segment, String functional_segment,
            String Fund_segment, String geo_segment, String dept_segment, int budget_year_id) {
//    public synchronized YearBudget exists(String admin_segment, String economic_segment) {
        YearBudget yearBudget = null;

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        try {
            //final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            //session.beginTransaction();
            tx = session.beginTransaction();
            Query q = session.createQuery("from YearBudget as a where a.admin_segment='" + admin_segment + "' and a.economic_segment='" + economic_segment
                    + "' and a.programme_segment='" + programme_segment + "' and a.functional_segment='" + functional_segment
                    + "' and a.Fund_segment='" + Fund_segment + "' and a.geo_segment='" + geo_segment
                    + "' and a.dept_id='" + dept_segment + "' and a.budget_year_id='" + budget_year_id + "'");
            //q.setMaxResults(1);
            yearBudget = (YearBudget) q.uniqueResult();
            //session.getTransaction().commit();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }
        return yearBudget;
    }

    public synchronized YearBudget exists(int id) {
        YearBudget btt = null;

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            Query q = session.createQuery("from " + TABLE_NAME + " as a where a.id = " + id);
            btt = (YearBudget) q.uniqueResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }

        return btt;
    }

    public synchronized String getMaxSerialNo(String tablename) {
        tablename = "dbo." + tablename;
        List recordlist = null;
        String resp = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        try {
            //final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            //session.beginTransaction();
            tx = session.beginTransaction();
            String sql = "select max(id) as maxserialno from " + tablename;
            SQLQuery q = session.createSQLQuery(sql);
            q.setMaxResults(1);
            q.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
            recordlist = q.list();
            HashMap hmap = (HashMap) recordlist.get(0);

            if (hmap.get("maxserialno") == null) {
                resp = "0";
            } else {
                resp = hmap.get("maxserialno").toString();
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }
        return resp;
    }

    public synchronized String getMaxSerialNo(String tablename, String code) {
        //System.out.println("code "+code);
        tablename = "dbo." + tablename;
        List recordlist = null;
        String resp = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        try {
            //final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            //session.beginTransaction();
            tx = session.beginTransaction();
            String sql = "select max(programme_segment) as maxserialno from " + tablename + " where programme_segment like '" + code + "%'";
            //System.out.println("sql "+sql);
            SQLQuery q = session.createSQLQuery(sql);
            q.setMaxResults(1);
            q.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
            recordlist = q.list();
            HashMap hmap = (HashMap) recordlist.get(0);

            if (hmap.get("maxserialno") == null) {
                resp = "0";
            } else {
                resp = hmap.get("maxserialno").toString();
            }
            switch (resp.length()) {
                case 1:
                    resp = code + "01";
                    break;
                case 12:
                    resp += "01";
                    break;
                default:
                    String resp_a = resp.substring(13);
                    int activity = Integer.parseInt(resp_a) + 1;
                    if (activity < 10) {
                        resp = code + "0" + activity;
                    } else {
                        resp = code + activity;
                    }
                    break;
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }
        //System.out.println("resp "+resp);
        return resp;
    }

    public synchronized String fetchAdminSegmment(String component_id, String admin_segment) {
        List objList = null;
        String jsonList = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            String sql = "select gl_account from Budget_Type_Components where id=" + component_id;

            SQLQuery q = session.createSQLQuery(sql);
            objList = q.list();

            String gl_code = objList.get(0).toString();
            switch (gl_code.length()) {
                case 1:
                    sql = "select code, name, (select sum(total_amount) FROM MTSS_MDA_Ceiling where mda_id='" + admin_segment + "' "
                            + " and budget_type_component_id='" + component_id + "' and budget_year_id=(select id from Budget_Years "
                            + "where is_current_base_year=1)+1) as envelope, (select year from Budget_Years where is_current_base_year=1)+1 "
                            + "as year_id from Economic_Segment_Header1 where code='" + gl_code + "'";
                    break;
                case 2:
                    sql = "select code, name, (select sum(total_amount) FROM MTSS_MDA_Ceiling where mda_id='" + admin_segment + "' "
                            + " and budget_type_component_id='" + component_id + "' and budget_year_id=(select id from Budget_Years "
                            + "where is_current_base_year=1)+1) as envelope, (select year from Budget_Years where is_current_base_year=1)+1 "
                            + "as year_id from Economic_Segment_Header2 where code='" + gl_code + "'";
                    break;
                case 4:
                    sql = "select code, name, (select sum(total_amount) FROM MTSS_MDA_Ceiling where mda_id='" + admin_segment + "' "
                            + " and budget_type_component_id='" + component_id + "' and budget_year_id=(select id from Budget_Years "
                            + "where is_current_base_year=1)+1) as envelope, (select year from Budget_Years where is_current_base_year=1)+1 "
                            + "as year_id from Economic_Segment_Header3 where code='" + gl_code + "'";
                    break;
                case 6:
                    sql = "select code, name, (select sum(total_amount) FROM MTSS_MDA_Ceiling where mda_id='" + admin_segment + "' "
                            + " and budget_type_component_id='" + component_id + "' and budget_year_id=(select id from Budget_Years "
                            + "where is_current_base_year=1)+1) as envelope, (select year from Budget_Years where is_current_base_year=1)+1 "
                            + "as year_id from Economic_Segment_Header4 where code='" + gl_code + "'";
                    break;
                default:
                    sql = "select code, name, (select sum(total_amount) FROM MTSS_MDA_Ceiling where mda_id='" + admin_segment + "' "
                            + " and budget_type_component_id='" + component_id + "' and budget_year_id=(select id from Budget_Years "
                            + "where is_current_base_year=1)+1) as envelope, (select year from Budget_Years where is_current_base_year=1)+1 "
                            + "as year_id from Economic_Segment where code in (" + gl_code + ")";
                    break;
            }
//System.out.println("sql  "+sql);

            q = session.createSQLQuery(sql);
            objList = q.list();
            Gson gson = new Gson();
            jsonList = gson.toJson(objList);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }

        return jsonList;
    }

    public synchronized String fetchEconSegmment(String econ_segment) {
        List objList = null;
        String jsonList = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            String sql = "select c.code, concat(c.name, ' [', c.code, ']') from Economic_Segment c where c.code like '" + econ_segment + "%'";
            if (econ_segment.contains(",")) {
                sql = "select c.code, concat(c.name, ' [', c.code, ']') from Economic_Segment c where c.code in (" + econ_segment + ")";
            }
            //System.out.println("sql   " + sql);
            SQLQuery q = session.createSQLQuery(sql);
            objList = q.list();
            Gson gson = new Gson();
            jsonList = gson.toJson(objList);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }

        return jsonList;
    }

    public synchronized String fetchProgSegmment() {
        List objList = null;
        String jsonList = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            String sql = "select c.code, concat(c.name, ' [', c.code, ']') from Programme_Segment c ";

            SQLQuery q = session.createSQLQuery(sql);
            objList = q.list();
            Gson gson = new Gson();
            jsonList = gson.toJson(objList);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }

        return jsonList;
    }

    public synchronized String fetchFuncSegmment() {
        List objList = null;
        String jsonList = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            String sql = "select c.code, concat(c.name, ' [', c.code, ']') from Functional_Segment c ";

            SQLQuery q = session.createSQLQuery(sql);
            objList = q.list();
            Gson gson = new Gson();
            jsonList = gson.toJson(objList);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }

        return jsonList;
    }

    public synchronized String fetchFundSegmment() {
        List objList = null;
        String jsonList = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            String sql = "select c.code, concat(c.name, ' [', c.code, ']') from Fund_Segment c ";

            SQLQuery q = session.createSQLQuery(sql);
            objList = q.list();
            Gson gson = new Gson();
            jsonList = gson.toJson(objList);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }

        return jsonList;
    }

    public synchronized String fetchGeoSegmment() {
        List objList = null;
        String jsonList = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            String sql = "select c.code, concat(c.name, ' [', c.code, ']') from Geographic_Segment c ";

            SQLQuery q = session.createSQLQuery(sql);
            objList = q.list();
            Gson gson = new Gson();
            jsonList = gson.toJson(objList);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }

        return jsonList;
    }

    public synchronized String fetchDept(String mda_id) {
        List objList = null;
        String jsonList = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            String sql = "select c.administrative_segment, concat(c.name, ' [', c.administrative_segment, ']') from Departments c where c.mda_id='" + mda_id + "'";

            SQLQuery q = session.createSQLQuery(sql);
            objList = q.list();
            Gson gson = new Gson();
            jsonList = gson.toJson(objList);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }

        return jsonList;
    }

    public synchronized String fetchAll(String admin_segment, String economic_segment, String budget_year_id) {
//System.out.println("admin_segment "+admin_segment+"     economic_segment "+economic_segment+"     budget_year_id "+budget_year_id);
        List yearBudgetsList = null;
        String jsonList = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        try {
            //final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            //session.beginTransaction();
            tx = session.beginTransaction();
            if (budget_year_id.equals("")) {
                budget_year_id = "0";
            }
            int year = Integer.parseInt(budget_year_id);
            //System.out.println("budget_year_id   "+budget_year_id);
            String sql = " select 1, c.code, c.name, "
                    + " (select ISNULL((select sum(b.AmountTc) from SAP_Actuals b where b.FiscYear=" + (year - 2) + " and b.CmmtItem=c.code and b.FundsCtr='" + admin_segment + "'),0)) as amount1, "
                    + " (select ISNULL((select sum(b.AmountTc) from SAP_Actuals b where b.FiscYear=" + (year - 1) + " and b.CmmtItem=c.code and b.FundsCtr='" + admin_segment + "'),0)) as amount2, 0 as amount3, 0 as amount4 "
                    + " from SAP_Actuals a, Economic_Segment c where a.CmmtItem=c.code and a.CmmtItem like '" + economic_segment + "%' and a.FundsCtr='" + admin_segment + "' "
                    + " and (select ISNULL((select sum(budget_amount) from Year_Budget where budget_year_id=" + (year - 1) + " and admin_segment='" + admin_segment + "' and economic_segment like '" + economic_segment + "%'),0))=0 "
                    + " and (select ISNULL((select sum(budget_amount) from Year_Budget where budget_year_id=" + year + " and admin_segment='" + admin_segment + "' and economic_segment like '" + economic_segment + "%'),0))=0 "
                    + " group by a.FundsCtr, a.CmmtItem, c.code, c.name "
                    + " UNION SELECT 1, c.code, c.name, "
                    + " (select ISNULL((select sum(AmountTc) from SAP_Actuals where FiscYear=" + (year - 2) + " and CmmtItem like '" + economic_segment + "%' and FundsCtr='" + admin_segment + "'),0)) as amount1, "
                    + " (select ISNULL((select sum(AmountTc) from SAP_Actuals where FiscYear=" + (year - 1) + " and CmmtItem like '" + economic_segment + "%' and FundsCtr='" + admin_segment + "'),0)) as amount2, "
                    + " (select ISNULL(sum(b.budget_amount),0) from Year_Budget b where b.admin_segment='" + admin_segment + "' "
                    + " and b.economic_segment like '" + economic_segment + "%' and b.budget_year_id=" + (year - 1) + ") as amount3, "
                    + " 0 as amount4 FROM Year_Budget a, Economic_Segment c where a.economic_segment=c.code and (select sum(b.budget_amount) FROM Year_Budget b where b.admin_segment='" + admin_segment + "' "
                    + " and b.economic_segment like '" + economic_segment + "%' and b.budget_year_id=" + year + ")=0 group by a.admin_segment, a.economic_segment, c.code, c.name "
                    + " UNION SELECT a.id, (select code from Economic_Segment where code=a.economic_segment) as code, "
                    + " (select name from Economic_Segment where code=a.economic_segment) as name, "
                    + " (select ISNULL((select sum(AmountTc) from SAP_Actuals where FiscYear=" + (year - 2) + " and CmmtItem like '" + economic_segment + "%' and FundsCtr='" + admin_segment + "'),0)) as amount1, "
                    + " (select ISNULL((select sum(AmountTc) from SAP_Actuals where FiscYear=" + (year - 1) + " and CmmtItem like '" + economic_segment + "%' and FundsCtr='" + admin_segment + "'),0)) as amount2, "
                    + " (select ISNULL(sum(b.budget_amount),0) from Year_Budget b where admin_segment='" + admin_segment + "' and economic_segment like '" + economic_segment + "%' and b.budget_year_id=" + (year - 1)
                    + " and a.admin_segment=b.admin_segment and a.programme_segment=b.programme_segment and a.economic_segment=b.economic_segment and a.functional_segment=b.functional_segment "
                    + " and a.fund_segment=b.fund_segment and a.geo_segment=b.geo_segment and a.dept_id=b.dept_id) as amount3, "
                    + " (select ISNULL(sum(b.budget_amount),0) from Year_Budget b where admin_segment='" + admin_segment + "' and economic_segment like '" + economic_segment + "%' and b.budget_year_id=" + year
                    + " and a.admin_segment=b.admin_segment and a.programme_segment=b.programme_segment and a.economic_segment=b.economic_segment and a.functional_segment=b.functional_segment "
                    + " and a.fund_segment=b.fund_segment and a.geo_segment=b.geo_segment and a.dept_id=b.dept_id) as amount4 "
                    + " FROM Year_Budget a where admin_segment='" + admin_segment + "' and economic_segment like '" + economic_segment + "%' "
                    + " group by a.budget_year_id,a.id,a.admin_segment,a.programme_segment,a.economic_segment,a.functional_segment,a.fund_segment,a.geo_segment,a.dept_id ";

            if (economic_segment.contains(",")) {
                sql = " select 1, c.code, c.name, "
                        + " (select ISNULL((select sum(b.AmountTc) from SAP_Actuals b where b.FiscYear=" + (year - 2) + " and b.CmmtItem=c.code and b.FundsCtr='" + admin_segment + "'),0)) as amount1, "
                        + " (select ISNULL((select sum(b.AmountTc) from SAP_Actuals b where b.FiscYear=" + (year - 1) + " and b.CmmtItem=c.code and b.FundsCtr='" + admin_segment + "'),0)) as amount2, 0 as amount3, 0 as amount4 "
                        + " from SAP_Actuals a, Economic_Segment c where a.CmmtItem=c.code and a.CmmtItem in (" + economic_segment + ") and a.FundsCtr='" + admin_segment + "' "
                        + " and (select ISNULL((select sum(budget_amount) from Year_Budget where budget_year_id=" + (year - 1) + " and admin_segment='" + admin_segment + "' and economic_segment in (" + economic_segment + ")),0))=0 "
                        + " and (select ISNULL((select sum(budget_amount) from Year_Budget where budget_year_id=" + year + " and admin_segment='" + admin_segment + "' and economic_segment in (" + economic_segment + ")),0))=0 "
                        + " group by a.FundsCtr, a.CmmtItem, c.code, c.name "
                        + " UNION SELECT 1, c.code, c.name, "
                        + " (select ISNULL((select sum(AmountTc) from SAP_Actuals where FiscYear=" + (year - 2) + " and CmmtItem in (" + economic_segment + ") and FundsCtr='" + admin_segment + "'),0)) as amount1, "
                        + " (select ISNULL((select sum(AmountTc) from SAP_Actuals where FiscYear=" + (year - 1) + " and CmmtItem in (" + economic_segment + ") and FundsCtr='" + admin_segment + "'),0)) as amount2, "
                        + " (select ISNULL(sum(b.budget_amount),0) from Year_Budget b where b.admin_segment='" + admin_segment + "' "
                        + " and b.economic_segment in (" + economic_segment + ") and b.budget_year_id=" + (year - 1) + ") as amount3, "
                        + " 0 as amount4 FROM Year_Budget a, Economic_Segment c where a.economic_segment=c.code and (select sum(b.budget_amount) FROM Year_Budget b where b.admin_segment='" + admin_segment + "' "
                        + " and b.economic_segment in (" + economic_segment + ") and b.budget_year_id=" + year + ")=0 group by a.admin_segment, a.economic_segment, c.code, c.name "
                        + " UNION SELECT a.id, (select code from Economic_Segment where code=a.economic_segment) as code, "
                        + " (select name from Economic_Segment where code=a.economic_segment) as name, "
                        + " (select ISNULL((select sum(AmountTc) from SAP_Actuals where FiscYear=" + (year - 2) + " and CmmtItem in (" + economic_segment + ") and FundsCtr='" + admin_segment + "'),0)) as amount1, "
                        + " (select ISNULL((select sum(AmountTc) from SAP_Actuals where FiscYear=" + (year - 1) + " and CmmtItem in (" + economic_segment + ") and FundsCtr='" + admin_segment + "'),0)) as amount2, "
                        + " (select ISNULL(sum(b.budget_amount),0) from Year_Budget b where admin_segment='" + admin_segment + "' and economic_segment in (" + economic_segment + ") and b.budget_year_id=" + (year - 1)
                        + " and a.admin_segment=b.admin_segment and a.programme_segment=b.programme_segment and a.economic_segment=b.economic_segment and a.functional_segment=b.functional_segment "
                        + " and a.fund_segment=b.fund_segment and a.geo_segment=b.geo_segment and a.dept_id=b.dept_id) as amount3, "
                        + " (select ISNULL(sum(b.budget_amount),0) from Year_Budget b where admin_segment='" + admin_segment + "' and economic_segment in (" + economic_segment + ") and b.budget_year_id=" + year
                        + " and a.admin_segment=b.admin_segment and a.programme_segment=b.programme_segment and a.economic_segment=b.economic_segment and a.functional_segment=b.functional_segment "
                        + " and a.fund_segment=b.fund_segment and a.geo_segment=b.geo_segment and a.dept_id=b.dept_id) as amount4 "
                        + " FROM Year_Budget a where admin_segment='" + admin_segment + "' and economic_segment in (" + economic_segment + ") "
                        + " group by a.budget_year_id,a.id,a.admin_segment,a.programme_segment,a.economic_segment,a.functional_segment,a.fund_segment,a.geo_segment,a.dept_id ";
            }

            //System.out.println("sql: "+sql);
            SQLQuery q = session.createSQLQuery(sql);
            yearBudgetsList = q.list();
//            Query q = session.createQuery("from YearBudget as a where a.name<>'' order by a.name");
//            yearBudgetsList =  q.list();
            //session.getTransaction().commit();
            Gson gson = new Gson();
            jsonList = gson.toJson(yearBudgetsList);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }
        return jsonList;
    }

    public synchronized String fetchAllProgrammes(String admin_segment, String economic_segment, String budget_year_id) {
//System.out.println("admin_segment "+admin_segment+"     economic_segment "+economic_segment+"     budget_year_id "+budget_year_id);
        List yearBudgetsList = null;
        String jsonList = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        try {
            //final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            //session.beginTransaction();
            tx = session.beginTransaction();
            if (budget_year_id.equals("")) {
                budget_year_id = "0";
            }
            int year = Integer.parseInt(budget_year_id);
            String sql = " SELECT id, concat(policy,code) as code, name, 0 as amt1, 0 as amt2, 0 as amt3, 0 as amt4, year_id, rank, '' as c_perc, '' as c_from, '' as c_to FROM Programme_Segment_Header3 "
                    + " where project_status=1  and mda in (select id from mdas where administrative_Segment='"+admin_segment+"')"
                    + " UNION select 1 as id, c.code, c.name,  (select ISNULL((select sum(b.AmountTc) from SAP_Actuals b where b.FiscYear=" + (year - 2)
                    + " and b.FundedProg=c.code and b.FundsCtr='" + admin_segment + "'),0)) as amount1,  (select ISNULL((select sum(b.AmountTc) from SAP_Actuals b "
                    + " where b.FiscYear=" + (year - 1) + " and b.FundedProg=c.code  and b.FundsCtr='" + admin_segment + "'),0)) as amount2, 0 as amount3, 0 as amount4, "
                    + " (select year_id from Programme_Segment_Header3 where code=substring(c.code,3,10)) as year_id, "
                    + " 100 as rank, '' as c_perc, '' as c_from, '' as c_to  from SAP_Actuals a, Programme_Segment_Header3 c where a.FundedProg=c.code and a.FundedProg like '" + economic_segment + "%' "
                    + " and a.FundsCtr='" + admin_segment + "' and (select ISNULL((select sum(budget_amount) from Year_Budget  where budget_year_id=" + (year - 1)
                    + " and admin_segment='" + admin_segment + "' and economic_segment like '" + economic_segment + "%'),0))=0  and (select ISNULL((select sum(budget_amount) "
                    + " from Year_Budget where budget_year_id=" + year + "  and admin_segment='" + admin_segment + "' and economic_segment like '" + economic_segment + "%'),0))=0 "
                    + " group by a.FundsCtr, a.FundedProg, c.code, c.name "
                    + " UNION SELECT 1 as id, c.code, c.name,  (select ISNULL((select sum(AmountTc) from SAP_Actuals where FiscYear=" + (year - 2) + "  and FundedProg like '" + economic_segment + "%' "
                    + " and FundsCtr='" + admin_segment + "'),0)) as amount1,  (select ISNULL((select sum(AmountTc)  from SAP_Actuals where FiscYear=" + (year - 1)
                    + " and FundedProg like '" + economic_segment + "%' and FundsCtr='" + admin_segment + "'),0)) as amount2,  (select ISNULL(sum(b.budget_amount),0) from Year_Budget b "
                    + " where b.admin_segment='" + admin_segment + "'  and b.economic_segment like '" + economic_segment + "%' and b.budget_year_id=" + (year - 1) + ") as amount3,  0 as amount4, "
                    + " (select year_id from Programme_Segment_Header3 where code=substring(c.code,3,10)) as year_id, 100 as rank, '' as c_perc, '' as c_from, '' as c_to  FROM Year_Budget a, Programme_Segment_Header3 c where a.economic_segment=c.code and (select sum(b.budget_amount) "
                    + " FROM Year_Budget b where b.admin_segment='" + admin_segment + "'  and b.economic_segment like '" + economic_segment + "%'  and b.budget_year_id=" + year + ")=0 "
                    + " group by a.admin_segment, a.economic_segment, c.code, c.name "
                    + " UNION SELECT a.id, a.programme_segment as code,  (select name from Programme_Segment_Header3 where code=substring(a.programme_segment,3,10)) as name, "
                    + " (select ISNULL((select sum(AmountTc) from SAP_Actuals where FiscYear=" + (year - 2) + " and FundedProg like '" + economic_segment + "%'  and FundsCtr='" + admin_segment + "'),0)) as amount1, "
                    + " (select ISNULL((select sum(AmountTc) from SAP_Actuals  where FiscYear=" + (year - 1) + " and FundedProg like '" + economic_segment + "%' and FundsCtr='" + admin_segment + "'),0)) as amount2, "
                    + " (select ISNULL(sum(b.budget_amount),0) from Year_Budget b where admin_segment='" + admin_segment + "'  and economic_segment like '" + economic_segment + "%' and b.budget_year_id=" + (year - 1)
                    + " and a.admin_segment=b.admin_segment  and a.programme_segment=b.programme_segment and a.economic_segment=b.economic_segment "
                    + " and a.functional_segment=b.functional_segment  and a.fund_segment=b.fund_segment  and a.geo_segment=b.geo_segment and a.dept_id=b.dept_id) as amount3, "
                    + " sum(a.budget_amount) as amount4, (select year_id from Programme_Segment_Header3 where code=substring(a.programme_segment,3,10)) as year_id, 100 as rank, ISNULL(a.percent_complete,''), ISNULL(a.complete_from,''), ISNULL(a.complete_to,'') "
                    + " FROM Year_Budget a where admin_segment='" + admin_segment + "' and economic_segment like '" + economic_segment + "%' "
                    + " group by a.budget_year_id,a.id,a.admin_segment,a.programme_segment,a.economic_segment,a.functional_segment,a.fund_segment,a.geo_segment,a.dept_id, a.percent_complete, a.complete_from, a.complete_to "
                    + " order by year_id, code, rank ";
            //System.out.println("sql: "+sql);
            SQLQuery q = session.createSQLQuery(sql);
            yearBudgetsList = q.list();
//            Query q = session.createQuery("from YearBudget as a where a.name<>'' order by a.name");
//            yearBudgetsList =  q.list();
            //session.getTransaction().commit();
            Gson gson = new Gson();
            jsonList = gson.toJson(yearBudgetsList);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }
        return jsonList;
    }

    public synchronized String fetchBudgetHeads() {
        List yearBudgetsList = null;
        String jsonList = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String sql = "select c.id, concat(c.name, ' [', c.gl_account, ']') from Budget_type_Components c where c.budget_type_id=2 AND c.is_budgeted=0 ORDER BY c.id";
            SQLQuery q = session.createSQLQuery(sql);
            yearBudgetsList = q.list();
            Gson gson = new Gson();
            jsonList = gson.toJson(yearBudgetsList);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }
        return jsonList;
    }

    public synchronized String fetchById(String id) {
        List yearBudgetsList = null;
        String jsonList = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String sql = "select a.* from year_budget a where id='" + id + "'";
            SQLQuery q = session.createSQLQuery(sql);
            yearBudgetsList = q.list();
            Gson gson = new Gson();
            jsonList = gson.toJson(yearBudgetsList);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }
        return jsonList;
    }

    public synchronized String fetchByUserRole(String userid, String userrole) {
        List subsectorsList = null;
        String jsonList = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String sql = "";
            if (userrole.contains("Admin")) {
                sql = "select a.id, concat(a.name, ' [', a.administrative_segment, ']') from Mdas a where a.sub_sector_code<>'00' order by a.name";
            } else {
                sql = "select a.id, concat(a.name, ' [', a.administrative_segment, ']') from Mdas a where  a.sub_sector_code<>'00' and a.id=(select mda_id from users where id='" + userid + "') order by a.sub_sector_code, a.name";
            }
            SQLQuery q = session.createSQLQuery(sql);
            subsectorsList = q.list();
            Gson gson = new Gson();
            jsonList = gson.toJson(subsectorsList);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }
        return jsonList;
    }

    @SuppressWarnings("unchecked")
    public synchronized List<YearBudget> fetchAll2(int budgetYearID, String sapBudgetType) {
        List<YearBudget> objList = null;

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            String sql = "select * from " + RAW_TABLE_NAME + " a "
                    + "where a.budget_year_id = " + budgetYearID + " and a.sap_document_number is NULL and a.id>6422 order by a.id";
            //and a.sap_budget_type = '" + sapBudgetType + "' 
//System.out.println("sql  "+sql);

            SQLQuery q = session.createSQLQuery(sql).addEntity(YearBudget.class);
            objList = (List<YearBudget>) q.list();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {

        }

        return objList;
    }

    public synchronized String updateAll(int budgetYearID, String sapBudgetType, String sapDocumentNumber) {
        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            String sql = "Update " + RAW_TABLE_NAME + " set sap_document_number = '" + sapDocumentNumber + "', sap_budget_type = '" + sapBudgetType + "' "
                    + "where a.budget_year_id = " + budgetYearID + " and  a.sap_document_number = NULL";
            //and a.sap_budget_type = '" + sapBudgetType + "' 
            session.createSQLQuery(sql).executeUpdate();

            tx.commit();
        } catch (HibernateException e) {
            System.out.println("error: " + e.getMessage());
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
            return Utility.ActionResponse.FAILED.toString();
        } finally {
            //session.close();
        }

        return Utility.ActionResponse.UPDATED.toString();
    }
    
    public synchronized String fetchBudgetVersions() {
        List yearBudgetsList = null;
        String jsonList = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String sql = "select a.* from Year_Budget_Versions a order by a.id";
            SQLQuery q = session.createSQLQuery(sql);
            yearBudgetsList = q.list();
            Gson gson = new Gson();
            jsonList = gson.toJson(yearBudgetsList);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }
        return jsonList;
    }

    public synchronized String fetchBudgetTypes() {
        List yearBudgetsList = null;
        String jsonList = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String sql = "select a.sap_budget_type,a.budget_type from Year_Budget_Types a order by a.sap_budget_type";
            SQLQuery q = session.createSQLQuery(sql);
            yearBudgetsList = q.list();
            Gson gson = new Gson();
            jsonList = gson.toJson(yearBudgetsList);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }
        return jsonList;
    }

    public synchronized String fetchBudgetCurrencies() {
        List yearBudgetsList = null;
        String jsonList = "";

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String sql = "select a.* from Currencies a order by a.id";
            SQLQuery q = session.createSQLQuery(sql);
            yearBudgetsList = q.list();
            Gson gson = new Gson();
            jsonList = gson.toJson(yearBudgetsList);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.close();
        }
        return jsonList;
    }


    /* YearBudget methods end */

}
