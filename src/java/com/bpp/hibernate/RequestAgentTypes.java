package com.bpp.hibernate;
// Generated May 1, 2018 6:26:16 AM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * RequestAgentTypes generated by hbm2java
 */
public class RequestAgentTypes  implements java.io.Serializable {


     private int id;
     private String name;
     private Date dateCreated;
     private Integer orgId;
     private Set requestAgentses = new HashSet(0);

    public RequestAgentTypes() {
    }

	
    public RequestAgentTypes(int id, String name, Date dateCreated) {
        this.id = id;
        this.name = name;
        this.dateCreated = dateCreated;
    }
    public RequestAgentTypes(int id, String name, Date dateCreated, Integer orgId, Set requestAgentses) {
       this.id = id;
       this.name = name;
       this.dateCreated = dateCreated;
       this.orgId = orgId;
       this.requestAgentses = requestAgentses;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public Date getDateCreated() {
        return this.dateCreated;
    }
    
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
    public Integer getOrgId() {
        return this.orgId;
    }
    
    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }
    public Set getRequestAgentses() {
        return this.requestAgentses;
    }
    
    public void setRequestAgentses(Set requestAgentses) {
        this.requestAgentses = requestAgentses;
    }




}


